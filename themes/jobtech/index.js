require('dotenv').config();
const fetch = (...args) => import('node-fetch').then(({default: fetch}) => fetch(...args));
const schedule = require('node-schedule');
let xml = require('xml');
fs = require('fs');
const sleep = require('util').promisify(setTimeout)
const EducationProvider = require('./classes/EducationProvider.js')
const EducationInfo = require('./classes/EducationInfo.js');
const { clearLine } = require('readline');
const EducationEvent = require('./classes/EducationEvent.js');
const EducatonList = require('./classes/EducationList.js');
const EventList = require('./classes/EventList.js');

const dec =  { declaration: { version: '1.0', encoding: 'UTF-8' }};

const doctype = `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">`
const isDevelopMode = false;
const retries = 10;
const pause = 0;
const nextSec = new Date().getSeconds()+3;
const nextMin = new Date().getMinutes();

let timeInterval = process.env.time_interval ? process.env.time_interval : `${nextSec} ${nextMin} * * * *`;

const getEventsByEducation = async (id) => {
    let events = await fetch(`https://ipf.arbetsformedlingen.se:443/tlrs-aub-for-asok/v1/tillfalle?client_id=${process.env.client_id}&client_secret=${process.env.client_secret}&select=*)&tlr_unique_utbildning_id=eq.${id}`, {
        "headers": {
            "af-environment": "PROD"
            },
            "body": null,
            "method": "GET",
            retry: retries,
            pause: pause
        }
    )
    
    let eventsJSON = await events.json();
    
    return eventsJSON;
}; 

const getEducation = async (id) => {
    let education = await fetch(`https://ipf.arbetsformedlingen.se:443/tlrs-aub-for-asok/v1/utbildning_uv_plats?&select=*,utbildningsmodul(*),utbildning_uv_plats_studiebesok(*))&tlr_unique_utbildning_id=eq.${id}&client_id=${process.env.client_id}&client_secret=${process.env.client_secret}`, {
        "headers": {
        "af-environment": "PROD"
        },
        "body": null,
        "method": "GET",
        retry: retries,
        pause: pause
    })
    let educationJSON = await education.json();
    return educationJSON
};

const getEducations = async () => {

        let educationsResponse = await fetch(`https://ipf.arbetsformedlingen.se:443/tlrs-aub-for-asok/v1/rpc/utbildning_list?&limit=9999&offset=0&offset=0&client_id=${process.env.client_id}&client_secret=${process.env.client_secret}`, {
            "headers": {
                "af-environment": "PROD",
                "content-type": "application/json",  
            },
            "method": "POST",
            retry: retries,
            pause: pause
        })
        const educationList = await educationsResponse.json();
       
        
        let html = new EducatonList(educationList);
        
        
        fs.writeFile('output/educationlist.html', xml({},dec)+xml(html,isDevelopMode), function (err) {
            if (err) return console.log(err);
            
        });
            
        return educationList
        
}
let deleteFolderRecursive = function(path) {
    if( fs.existsSync(path) ) {
        fs.readdirSync(path).forEach(function(file) {
          let curPath = path + "/" + file;
            if(fs.lstatSync(curPath).isDirectory()) { 
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
      }
  };
job = schedule.scheduleJob(timeInterval, async function(){
    //delete old output
    let events = [];
    try {
        fs.readFile('./static/providerlist.html', 'utf8', (err, data) => {
            if (err) {
              console.error(err);
              return;
            }
            fs.writeFile(`output/providerlist.html`, data , function (err) {
                if (err) return console.log(err);
                
            });
        });
        
        deleteFolderRecursive('./output')

      } catch(err) {
        console.error(err)
      }
      let path = './output';
      if (!fs.existsSync(path)){
          fs.mkdirSync(path, { recursive: true });
      }
   
    const provider = new EducationProvider();


    fs.writeFile(`output/provider.xml`, xml(provider,isDevelopMode), function (err) {
        if (err) return console.log(err);
    
    });
    try {
    let educationList = await getEducations();
    
    for await (const element of educationList) {
        await sleep(pause);
        
        let education = await getEducation(element.tlr_unique_utbildning_id);
        education = education[0];
        let path = `output/${element.tlr_unique_utbildning_id.slice(0,3)}/${element.tlr_unique_utbildning_id}/`;

        //todo urskilja grupppstart osv
        let eventsForEducation = await getEventsByEducation(education.tlr_unique_utbildning_id);
        
        for(e of eventsForEducation) {
            
            events.push({
                tlr_unique_utbildning_id:element.tlr_unique_utbildning_id,
                tlr_unique_tillfalle_id:e.tlr_unique_tillfalle_id,
                studietakt:e.studietakt,
                utbilningsnamn:element.utbildningsnamn


            })
        }



        
        eventsForEducation.forEach(event => {
            let educationEvent = new EducationEvent(education, event)
            if (!fs.existsSync(path)){
                fs.mkdirSync(`${path}/events`, { recursive: true });
            }
            fs.writeFile(`${path}/events/${event.tlr_unique_tillfalle_id}.xml`, xml({},dec)+xml(educationEvent,isDevelopMode), function (err) {
                if (err) return console.log(err);
                
            });
            
            
        })
        if (!fs.existsSync(path)){
            fs.mkdirSync(path, { recursive: true });
        }
        if (!fs.existsSync(path)){
            fs.mkdirSync(`${path}/events`, { recursive: true });
        }

        let fileName = `${path}education.xml`;
        education = new EducationInfo(education);
        
        
        fs.writeFile(fileName, xml(education, isDevelopMode), function (err) {
            if (err) return console.log(err);
            
        });
    }
    console.log(events)
    let html = new EventList(events);

    fs.writeFile(`output/eventlist.html`, xml({},dec)+xml(html,isDevelopMode), function (err) {
        if (err) return console.log(err);
        
    });
}catch(err) {
    console.error(err);
}

console.log("done");

});
     








