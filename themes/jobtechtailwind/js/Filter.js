class Filter {
    constructor(selector) {

        

        
        const elementList = document.querySelectorAll(`.${selector}`);
        let availableTags = [];

        for (let element of elementList) {
            availableTags = availableTags.concat(element.getAttribute('data-tags').split(','));
        }
        this.selector = selector;
        this.elementList = elementList;
    }

    getRegex() {
        let suffix = `)(,|$))`
        let prefix = `(?=.*(^|,)+(`
        

        return new RegExp(prefix + this.getParams().join(suffix+prefix) + suffix, 'i');
    }

    setUnavalibleTags() {
        const availableElements = document.querySelectorAll(`.${this.selector}:not(.hidden)`);
        let avaibableTags = [];

        for (let elem of availableElements) {
            avaibableTags = avaibableTags.concat(elem.getAttribute("data-tags").split(','));
        }

        avaibableTags = new Set([...avaibableTags]);
        
        for (let elem of document.querySelectorAll(".tag-button")) {
            console.log(elem)
            console.log(decodeURIComponent(elem.id))
            elem.disabled = avaibableTags.has(decodeURIComponent(elem.id)) ? false : true;
            elem.classList.toggle("unavalible", !avaibableTags.has(decodeURIComponent(elem.id)))
        }
    }

    getParams() {
        const params = new URLSearchParams(document.location.search);

        if (params.has('q')) {
            let qs = params.get('q').split(',').filter(value => value !== '');
            return qs;
        }

        return [];
    }

    chooseTag(tag) {
        let tags = this.getParams();

        if (tags.includes(tag)) {
            tags = tags.filter(item => item != tag);
        } else {
            tags.push(tag);
        }

        history.replaceState(null, null, "?q=" + tags.join(","))
        this.updateElements();


    }

    updateElements() {
       
            for (let elem of this.elementList) {
                elem.classList.toggle("hidden", !this.getRegex().test(elem.getAttribute("data-tags")) && this.getParams().length != 0);
            }
            
            
        
            for(let button of document.querySelectorAll(".tag-button")){
                console.log(button.innerHTML)
                button.classList.toggle("bg-black", this.getParams().includes(button.innerHTML))
                button.classList.toggle("text-white", this.getParams().includes(button.innerHTML))
            }
        
        this.setUnavalibleTags();
        
        

    }

}
document.addEventListener('DOMContentLoaded', function() {
	
	filter = new Filter("listitem");
	
	
	filter.updateElements()
}, false)

