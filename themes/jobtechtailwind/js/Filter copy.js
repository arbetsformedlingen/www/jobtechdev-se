class Filter{
    constructor(classOfListItem){
        let availableTags = [];
        let elementList = document.querySelectorAll(`.${classOfListItem}`);
        for(let element of elementList){
            availableTags = availableTags.concat(element.getAttribute('data-tags').split(','));
        }
        
        // for(let tag of new Set(availableTags)){
        //     tags.push({
        //         name: tag,
        //         status: 'avaibable',
        //         elements: function (){
        //             let arr =[]
        //             for(let element of elementList){
        //                 for(let datatag of element.getAttribute('data-tags').split(',')){
        //                     if(datatag === this.name){
        //                         arr.push(element);
        //                     }
        //                 }
        //             }
        //             return arr;
        //         }
                
        //     })
        // }
        // this.unavailableTags = [];
        // this.matches = [];
        this.elementList = elementList;
        // this.tags = tags;  
         
    }
    getRegex () {
        let paramsArray = this.getParams();
        let regexStr = "";
        for(let value of paramsArray){
            regexStr += `(?=.*(^|,)+(${value})(,|$))`;
                        
        }

        const tagRegex = new RegExp(`${regexStr}`, 'i'); // e.g. /(?|,)+(api|applikation)(,|$)/      https://regex101.com/
        // (?=.*(^|,)+(app)(,|$))(?=.*(^|,)+(api)(,|$))
        
        return tagRegex;
    }
    setUnavalibleTags(){
        
        let availableElements = document.querySelectorAll(".listitem:not(.hidden)");
        let avaibableTags = []


        for(let elem of availableElements){
            avaibableTags = avaibableTags.concat(elem.getAttribute("data-tags").split(','));
            
        }
        
        avaibableTags = new Set([...avaibableTags]);
        console.log(avaibableTags)
        for(let elem of document.querySelectorAll(".tag-button")){
            elem.disabled = avaibableTags.has(elem.id)? false:true;
        }
        
    }
    // setMatchingElements () {
    //     let matches = [];
    //     let paramsArray = this.getParams();
    //     let regexStr = "";
    //     for(let value of paramsArray){
    //         regexStr += `(?=.*(^|,)+(${value})(,|$))`;
    //     }

    //     const tagRegex = new RegExp(`${regexStr}`, 'i'); // e.g. /(?|,)+(api|applikation)(,|$)/      https://regex101.com/
    //     // (?=.*(^|,)+(app)(,|$))(?=.*(^|,)+(api)(,|$))
        
    //     for(let elem of this.elementList){

            
    //         if(tagRegex.test(elem.getAttribute("data-tags"))){
                
    //             matches.push(elem);
    //         }
        
    //     }
    //     this.matches = matches;
    // }
    getParams(){
        let params =  new URLSearchParams(document.location.search);
        if(params.has('q')){
            let qs = params.get('q').split(',').filter(value => value != '');
            return qs;
        }
        return [];
    }
    chooseTag(tag) {
        
       let tags = this.getParams();
       
       if(tags.includes(tag)){
        tags = tags.filter(item => item != tag);
       }else{tags.push(tag)}
       
       
        history.replaceState(null ,null, "?q="+tags.join(",") )
        this.updateElements();
        
    }
    
    updateElements(){
        
        
        for(let elem of this.elementList){
            elem.classList.toggle("hidden", !this.getRegex().test(elem.getAttribute("data-tags")));
        }
        this.setUnavalibleTags()
        //for(let elem of this.matches){
        //    elem.classList.add("active");
        //}

        
    }
    
    


}


